﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public Transform tf;

    public float linealForce;
    public float horizontalForce;
    public float angularForce;
    public float jumpForce;

    public bool onGround;
    public bool jumping;
    [SerializeField]
    bool inTube;
    bool blockJump;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start(){
        rb = GetComponent<Rigidbody>();
        
    }

    private void FixedUpdate()
    {
        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");

        if (inTube){
            rb.centerOfMass = tf.localPosition;
            Vector3 angles = gameObject.transform.rotation.eulerAngles;
            float inclination = angles.z;
            if (inclination > 180) inclination = Mathf.Abs(inclination-360);
            if (inclination >= 30) rb.constraints = RigidbodyConstraints.None;
            else MovementInTube(v, h);
        }
        else
        {
            rb.AddForce(v * transform.forward * linealForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
            rb.AddForce(h * transform.right * horizontalForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
            if (v == 0) rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0);
            if (h == 0) rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
        }
    }

    private void MovementInTube(float z, float ang) {

        rb.AddTorque(-ang * transform.forward * angularForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
        rb.AddForce(z * transform.forward * linealForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
        if (z == 0) rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0);
        if (ang == 0) rb.angularVelocity = new Vector3(rb.angularVelocity.x, rb.angularVelocity.y, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.other.gameObject.layer == 8) {
            rb.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
            inTube = true;
            blockJump = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.other.gameObject.layer == 8){
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            inTube = false;
            blockJump = false;
            rb.MoveRotation(Quaternion.identity);
        }
    }
    
}
